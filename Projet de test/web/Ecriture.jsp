<%-- 
    Document   : Ecriture.jsp
    Created on : 7 nov. 2022, 19:51:50
    Author     : Mitasoa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body style="font-size: 15px;">
        <h1>Vous voulez tester la magie de ce framework ?</h1>
        <form method="POST" action="ecriture-add.do">
            <input style="font-size: 18px; margin: 1%;" type="number" name="id" placeholder="un nombre ?" required><br>
            <input style="font-size: 18px; margin: 1%;" type="date" name="date" placeholder="" required><br>
            <input style="font-size: 18px; margin: 1%;" type="text" name="ref" placeholder="une référence ?" required><br>
            <input style="font-size: 18px; margin: 1%;" type="number" name="exerciceId" placeholder="encore un nombre?" required><br>
            <input style="font-size: 18px; margin: 1%;" type="number" name="journalId" placeholder="toujours un nombre?" required><br>
            <input style="font-size: 18px; margin: 1%;" type="submit" value="VOIR CELA">
        </form>
    </body>
</html>
