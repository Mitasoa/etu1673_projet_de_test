/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import annotation.Url;
import java.util.HashMap;
import model.ViewModel;

/**
 *
 * @author Mitasoa
 */
public class Ecriture {
    private Integer id;
    private String date;
    private String ref;
    private Integer exerciceId;
    private Integer journalId;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the ref
     */
    public String getRef() {
        return ref;
    }

    /**
     * @param ref the ref to set
     */
    public void setRef(String ref) {
        this.ref = ref;
    }

    /**
     * @return the exerciceId
     */
    public Integer getExerciceId() {
        return exerciceId;
    }

    /**
     * @param exerciceId the exerciceId to set
     */
    public void setExerciceId(Integer exerciceId) {
        this.exerciceId = exerciceId;
    }

    /**
     * @return the journalId
     */
    public Integer getJournalId() {
        return journalId;
    }

    /**
     * @param journalId the journalId to set
     */
    @Url(url="ecriture-select")
    public ViewModel info(){
        ViewModel retour = new ViewModel();
        Object succes = "Si le possible n'a rien donnée , alors tetons l'impossible";
        retour.setJsp("Success.jsp");
        retour.setCall("success");
        HashMap <String,Object> data = new HashMap <String,Object>();
        data.put("success", succes);
        retour.setData(data);
        return retour;
    }
    
    
    @Url(url="ecriture-add")
    public ViewModel Save(){
        ViewModel retour = new ViewModel();
        Object __ecriture = this;
        HashMap <String,Object> data = new HashMap <String,Object>();
        data.put("ecriture", __ecriture);
        retour.setJsp("EcritureInsert.jsp");
        retour.setCall("insertion");
        retour.setData(data);
        return retour;
    }

    /**
     * @param journalId the journalId to set
     */
    public void setJournalId(Integer journalId) {
        this.journalId = journalId;
    }
}
